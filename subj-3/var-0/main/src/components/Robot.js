import React, { Component } from 'react'

class Robot extends Component {
    delete = () => {
        this.props.onDelete(this.props.item.id)
    }
    render() {
        return (
            <div>
                <p>{this.props.item.name}</p>
                <button onClick={this.delete} value="delete">Delete</button>
            </div>
        )
    }
}

export default Robot
